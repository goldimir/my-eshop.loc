/*
 * Validation
 */
jQuery.extend(jQuery.fn, {

    validate_form: function (data) {
        let $this = $(this);

        let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
            emailToValidate = $this.val();

        let phoneReg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
            phoneToValidate = $this.val();

        let numReg = /^\d+(\.\d+)*$/,
            numToValidate = $this.val();

        if ($this.val().length < 3 && data === 'min-symbol') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.addClass('error');
            $this.removeClass('valid');
            $this.parent().append('<div>Please type something</div>').find('div').addClass('error').hide().fadeIn(600);
        } else if ((!numReg.test(numToValidate) || numToValidate === "") && data === 'price') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.parent().addClass('error');
            $this.parent().removeClass('valid');
            $this.parent().append('<div class="error">Type valid Price</div>').find('div.error').hide().fadeIn(600);
        } else if ($this.val().length === 0 && data === 'arr') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.addClass('error');
            $this.removeClass('valid');
            $this.parent().append('<div>Select something</div>').find('div').addClass('error').hide().fadeIn(600);
        } else if (((!numReg.test(numToValidate) || numToValidate === "") || $this.val().length !== 4) && data === 'year') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.addClass('error');
            $this.removeClass('valid');
            $this.parent().append('<div>Type valid Year</div>').find('div').addClass('error').hide().fadeIn(600);
        } else if ((!emailReg.test(emailToValidate) || emailToValidate === "") && data === 'email') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.addClass('error');
            $this.removeClass('valid');
            $this.parent().append('<div>Type valid Email address</div>').find('div').addClass('error').hide().fadeIn(600);
        } else if (!phoneReg.test(phoneToValidate) && phoneToValidate && data === 'phone') {
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            $this.addClass('error');
            $this.removeClass('valid');
            $this.parent().append('<div>Type valid number of phone</div>').find('div').addClass('error').hide().fadeIn(600);
        } else {
            $this.removeClass('error');
            $this.removeClass('valid');
            $this.parent().find('div.error').remove();
            $this.parent().find('div.valid').remove();
            if (data === 'price') {
                $this.parent().removeClass('error');
                $this.parent().addClass('valid');
                $this.parent().append('<div class="valid">Valid</div>').find('div.valid').hide().fadeIn(600);
            } else {
                $this.addClass('valid');
                $this.parent().append('<div>Valid</div>').find('div').addClass('valid').hide().fadeIn(600);
            }
            return true;
        }
    },
});

/**
 * Filter Books
 */
let searchVal    = $('.books-catalog'),
    form         = $('.vg-filter'),
    selectFilter = $('.select-filter'),
    resetBtn     = $('.reset-btn'),
    selectObj    = {};

form.on('change',function () {

    $(this).find(':selected').each(function() {
        selectObj[$(this).data('book')] = $(this).val();
    });

    if (!$.isEmptyObject(selectObj.genre) || !$.isEmptyObject(selectObj.author))
        resetBtn.fadeIn(600);

    let adminChk = false;
    if (window.location.href.indexOf("admin") > -1)
        adminChk = true;

    $.ajax({
        url: (adminChk === false) ? '/inc/ajax-books-list.php' : '/admin/admin-ajax-books-list.php',
        type: 'POST',
        data: {
            'term': selectObj,
        },
        beforeSend: function () {
            searchVal.addClass("loading");
        },
        error: function (request, status, error) {
            if (status === 500) {
                alert('Error while sending');
            } else if (status === 'timeout') {
                alert('Error: Server doesn\'t respond.');
            } else {
                let wpErrorHtml = request.responseText.split("<p>"),
                    wpErrorStr = wpErrorHtml[1].split("</p>");

                alert(wpErrorStr[0]);
            }
        },
        complete: function () {
            searchVal.removeClass("loading");

            $('[data-toggle="tooltip"]').tooltip();

            if(adminChk === true) {
                $('.list-book .del-book-btn').on('click', function () {
                    let id = $(this).data('id');
                    $('#modal-del-book .modal-body a').attr({href: 'del-book.php?del=' + id});
                });

                $('.list-book .edit-book-btn').on('click', function () {
                    let bookId = $(this).data('id');

                    $.ajax({
                        url: 'show-edit-book.php',
                        context: $(this),
                        type: 'POST',
                        data: {
                            'id': bookId,
                        },
                        error: function (request, status, error) {
                            if (status === 500) {
                                alert('Error while sending');
                            } else if (status === 'timeout') {
                                alert('Error: Server doesn\'t respond.');
                            } else {
                                let wpErrorHtml = request.responseText.split("<p>"),
                                    wpErrorStr = wpErrorHtml[1].split("</p>");
                                alert(wpErrorStr[0]);
                            }
                        },
                        complete: function () {
                            updateFormValidate($('#form-edit-book'));
                        },
                        success: function (resultHTML) {
                            $('#modal-edit-book').children().html(resultHTML);
                        }
                    });
                });
            }

        },
        success: function (result) {
            searchVal.hide().fadeIn(600).html(result);
        }
    });
});

resetBtn.on('click', function (e) {
    selectFilter.val('');
    form.trigger('change');
    $(this).fadeOut(600);
    e.preventDefault();
});

/*
 * Add new book form check
 */
let addBookForm = $('#form-add-book');
addBookForm.on('submit', function (e) {

    let submit      = $('.book-submit', this),
        title       = $('.title', this),
        pubyear     = $('.pubyear', this),
        price       = $('.price', this),
        description = $('.description', this),
        genres      = $('.genres', this),
        authors     = $('.authors', this);

    title.validate_form('min-symbol');
    pubyear.validate_form('year');
    price.validate_form('price');
    description.validate_form('min-symbol');
    genres.validate_form('arr');
    authors.validate_form('arr');

    // if form isn't in process, submit it
    if (!submit.hasClass('loadingform') && !title.hasClass('error') && !pubyear.hasClass('error') && !price.hasClass('error') && !description.hasClass('error') && !genres.hasClass('error') && !authors.hasClass('error')){
        $.ajax({
            url: 'save-book.php',
            context: $(this),
            type: 'POST',
            data: {
                'title': title.val(),
                'pubyear': pubyear.val(),
                'price': price.val(),
                'description': description.val(),
                'genres': genres.val(),
                'authors': authors.val(),
            },
            error: function (request, status, error) {
                if (status === 500) {
                    alert('Error while sending');
                } else if (status === 'timeout') {
                    alert('Error: Server doesn\'t respond.');
                } else {
                    let wpErrorHtml = request.responseText.split("<p>"),
                        wpErrorStr = wpErrorHtml[1].split("</p>");
                    alert(wpErrorStr[0]);
                }
            },
            success: function () {
                $('#addBookModalCenter').modal('hide');
                $('.vg-filter').trigger('change');
                $('body').prepend('<div class="send-success">New book has been added</div>').find('.send-success').hide().slideDown(400);
                $('div.send-success').delay(5000).slideUp(300, function() {
                    $(this).remove();
                });
                $(this).trigger("reset");
            }
        });
        return false;
    }

    e.preventDefault();
});

/*
 * UPDATE book form check
 */
function updateFormValidate (editBookForm) {

    editBookForm.on('submit', function (e) {

        let submit      = $('.book-submit', this),
            idBook      = $('.id', this),
            title       = $('.title', this),
            pubyear     = $('.pubyear', this),
            price       = $('.price', this),
            description = $('.description', this),
            genres      = $('.genres', this),
            authors     = $('.authors', this);

        title.validate_form('min-symbol');
        pubyear.validate_form('year');
        price.validate_form('price');
        description.validate_form('min-symbol');
        genres.validate_form('arr');
        authors.validate_form('arr');

        // if form isn't in process, submit it
        if (!submit.hasClass('loadingform') && !title.hasClass('error') && !pubyear.hasClass('error') && !price.hasClass('error') && !description.hasClass('error') && !genres.hasClass('error') && !authors.hasClass('error')){
            $.ajax({
                url: 'edit-book.php',
                context: $(this),
                type: 'POST',
                data: {
                    'id': idBook.val(),
                    'title': title.val(),
                    'pubyear': pubyear.val(),
                    'price': price.val(),
                    'description': description.val(),
                    'genres': genres.val(),
                    'authors': authors.val(),
                },
                error: function (request, status, error) {
                    if (status === 500) {
                        alert('Error while sending');
                    } else if (status === 'timeout') {
                        alert('Error: Server doesn\'t respond.');
                    } else {
                        let wpErrorHtml = request.responseText.split("<p>"),
                            wpErrorStr = wpErrorHtml[1].split("</p>");
                        alert(wpErrorStr[0]);
                    }
                },
                success: function () {
                    $('#modal-edit-book').modal('hide');
                    $('body').prepend('<div class="send-success">Book updated</div>').find('.send-success').hide().slideDown(400);
                    $('div.send-success').delay(5000).slideUp(300, function() {
                        $(this).remove();
                    });
                    // $('.vg-filter').trigger('change');
                }
            });
            return false;
        }

        e.preventDefault();
    });
}

/*
 * Send mail form check
 */
// define some vars
let mailForm   = $('#send-book-form'),
    mailButton = $('#send-book-submit'),
    bookId     = $('#book_id'),
    name       = $('#name'),
    email      = $('#email'),
    quantity   = $('#quantity');

mailForm.on('submit', function (e) {

    name.validate_form('min-symbol');
    email.validate_form('email');

    // if form isn't in process, submit it
    if (!mailButton.hasClass('loadingform') && !name.hasClass('error') && !email.hasClass('error')){
        $.ajax({
            url: 'send-book.php',
            context: $(this),
            type: 'POST',
            data: {
                'id': bookId.val(),
                'name': name.val(),
                'email': email.val(),
                'quantity': quantity.val(),
            },
            error: function (request, status, error) {
                if (status === 500) {
                    alert('Error while sending');
                } else if (status === 'timeout') {
                    alert('Error: Server doesn\'t respond.');
                } else {
                    let wpErrorHtml = request.responseText.split("<p>"),
                        wpErrorStr = wpErrorHtml[1].split("</p>");
                    alert(wpErrorStr[0]);
                }
            },
            success: function () {
                $(this).find('div.valid').fadeOut(600, function() {
                    $(this).remove();
                });
                $('body').prepend('<div class="send-success">Message has been sent</div>').find('.send-success').hide().slideDown(400);
                $('div.send-success').delay(5000).slideUp(300, function() {
                    $(this).remove();
                });
                $(this).trigger("reset");
            }
        });
        return false;
    }

    e.preventDefault();
});

// Get id from url
// function getUrlParameters(parameter, staticURL, decode){
//     var currLocation = (staticURL.length)? staticURL : window.location.search,
//         parArr = currLocation.split("?")[1].split("&"),
//         returnBool = true;
//     for(var i = 0; i < parArr.length; i++){
//         parr = parArr[i].split("=");
//         if(parr[0] === parameter){
//             return (decode) ? decodeURIComponent(parr[1]) : parr[1];
//         }else{
//             returnBool = false;
//         }
//     }
//     if(!returnBool) return false;
// }
