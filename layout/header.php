<?php

$adminPath = '../inc/connect-bd.php';
require_once (file_exists($adminPath)) ? $adminPath : "inc/connect-bd.php";
$adminPath = '../inc/lib.php';
require_once (file_exists($adminPath)) ? $adminPath : "inc/lib.php";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Book Shop</title>
    <link rel='shortcut icon' type='image/x-icon' href='/assets/i/favicon.ico' />
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.all.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="header"></div>
        </div>
    </div>