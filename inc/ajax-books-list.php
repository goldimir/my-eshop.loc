<?php
	require "connect-bd.php";
	require "lib.php";
?>
<table class="table table-sm table-striped table-hover table-bordered list-book">
    <thead class="thead-light">
        <tr>
            <th>Title</th>
            <th>Published</th>
            <th>Price</th>
            <th>Purchase</th>
        </tr>
    </thead>
    <?php
    $selectResult = ajax_filter_search( $_POST['term'] );
    while($view = mysqli_fetch_assoc( $selectResult )){
  ?>
        <tr>
            <td><?=$view["title"]?></td>
            <td><?=$view["pubyear"]?></td>
            <td>$<?=$view["price"]?></td>
            <td>
                <a class="btn btn-primary btn-sm buy-book" data-toggle="tooltip" data-placement="top" href="/inc/purchase-book.php?id=<?=$view["id"]?>" title="Buy"><i class="fas fa-shopping-bag"></i></a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
