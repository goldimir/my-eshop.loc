<?php
require "connect-bd.php";
require "lib.php";

$bookId   = clearData($_POST['id'],'int');
$email    = clearData($_POST['email']);
$name     = clearData($_POST['name']);
$quantity = clearData($_POST['quantity'],'int');

$resultBookDetails = bookDetailById($bookId);
$bookView = mysqli_fetch_assoc( $resultBookDetails );

$resultBookGenres = bookGenresById($bookId);
while($view = mysqli_fetch_assoc( $resultBookGenres )) {
    $genres[] = $view['genres_name'];
}

$resultBookAuthors = bookAuthorsById($bookId);
while($view = mysqli_fetch_assoc( $resultBookAuthors )) {
    $authors[] = $view['authors_name'];
}

$authors = (isset($authors))?implode(", ", $authors):'';
$genres  = (isset($genres))?implode(", ", $genres):'';

// Send an email
$to = 'warlock@geeksforless.net';

$subject = 'Order from books shop';

$eol = "\r\n";
$headers = "From: " . $email . $eol;
$headers .= "Reply-To: ". strip_tags($email) . $eol;
$headers .= "MIME-Version: 1.0" . $eol;
$headers .= "Content-Type: text/html; charset=ISO-8859-1" . $eol;

$message = '<html><body>';
$message .= '<table rules="all" style="border: 1px solid #7d7d7d;" cellpadding="7">' . $eol;
$message .= "<tr style='background: #eee;'><td colspan='2'><h2 style='margin: 0;'>Customer</h2></td></tr>" . $eol;
$message .= "<tr><td><strong>Name:</strong> </td><td>" . strip_tags($name) . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Number of copies:</strong> </td><td>" . $quantity . "</td></tr>" . $eol;
$message .= "<tr style='background: #eee;'><td colspan='2'><h2 style='margin: 0;'>Book</h2></td></tr>" . $eol;
$message .= "<tr><td><strong>Title:</strong> </td><td>" . $bookView["title"] . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Author(s):</strong> </td><td>" . $authors . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Genre(s):</strong> </td><td>" . $genres . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Published:</strong> </td><td>" . $bookView["pubyear"] . "</td></tr>" . $eol;
$message .= "<tr><td><strong>Price:</strong></td><td>$" . $bookView["price"] . "</td></tr>" . $eol;
$message .= "</table>" . $eol;
$message .= "</body></html>";

mail($to, $subject, $message, $headers);

?>