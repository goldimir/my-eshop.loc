<?php
require_once '../layout/header.php';

$bookId = clearData($_GET['id'],'int');

$resultBookDetails = bookDetailById($bookId);
$bookView = mysqli_fetch_assoc( $resultBookDetails );

$resultBookGenres = bookGenresById($bookId);
while($view = mysqli_fetch_assoc( $resultBookGenres )) {
    $genres[] = $view['genres_name'];
}

$resultBookAuthors = bookAuthorsById($bookId);
while($view = mysqli_fetch_assoc( $resultBookAuthors )) {
    $authors[] = $view['authors_name'];
}
?>
    <div class="row">
        <div class="col-sm-12"><div class="separator"></div></div>
        <div class="col-md-6">
            <h2><?=$bookView["title"]?></h2>

            <h5>Description</h5>
            <p><?=$bookView["description"]?></p>

            <h5>Details</h5>
            <table class="table table-sm table-striped table-bordered">
                <tr>
                    <th>Genres</th>
                    <td><?=(isset($genres))?implode(", ", $genres):''?></td>
                </tr>
                <tr>
                    <th>Authors</th>
                    <td><?=(isset($authors))?implode(", ", $authors):''?></td>
                </tr>
                <tr>
                    <th>Published</th>
                    <td><?=$bookView["pubyear"]?></td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td>$<?=$bookView["price"]?></td>
                </tr>
            </table>
            <a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-dark">Back</a>
        </div>
        <div class="col-md-6">
            <h2>Buy options</h2>
            <form id="send-book-form" action="send-book.php" method="post">
                <input name="id" id="book_id" type="hidden" value="<?=$bookId?>" />
                <div class="form-group"><input class="form-control" id="name" type="text" name="name" placeholder="Name"></div>
                <div class="form-group"><input class="form-control" id="email" type="email" name="email" placeholder="Email"></div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Quantity</span>
                    </div>
                    <input class="form-control" id="quantity" name="quantity" type="number" value="1" min="1" max="100" step="1">
                </div>
                <div class="form-group">
                    <button type="submit" id="send-book-submit" class="btn btn-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
<?php require_once '../layout/footer.php'; ?>