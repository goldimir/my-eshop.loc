<?php
function clearData($data, $type="str"){
    global $link;
    switch($type){
        case "str":
            $data = mysqli_real_escape_string($link, trim(strip_tags($data)));break;
        case "int":
            $data = trim(abs(strip_tags($data)));break;
        case "arr":
            if (is_array($data)){ break; }
            $data = [];break;
        case "prc":
            $pattern = '/^\d+(\.\d{2})?$/';
            if (preg_match($pattern, $data) == '0') {
                $data = 0;break;
            }
            $data = number_format(trim(strip_tags($data)), 2, '.', '');break;
    }
    return $data;
}

function selectAll($table){
    global $link;
    $sql = "SELECT * FROM $table ORDER BY name";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    return($result);
}

function addBook($data1, $data2, $data3, $data4, $data5, $data6){
    global $link;
    $sql = "INSERT INTO books 
            (title, description, pubyear, price) 
            VALUE 
            ('$data1', '$data2', $data3, $data4)";
    mysqli_query($link, $sql) or die(mysqli_error($link));

    $id_book = mysqli_insert_id($link);

    foreach ($data5 as $id_genre){
        $sql = "INSERT INTO books_genres 
                (id_genre, id_book) 
                VALUE 
                ($id_genre, $id_book)";
        mysqli_query($link, $sql) or die(mysqli_error($link));
    }

    foreach ($data6 as $id_author){
        $sql = "INSERT INTO books_authors 
                (id_author, id_book) 
                VALUE 
                ($id_author, $id_book)";
        mysqli_query($link, $sql) or die(mysqli_error($link));
    }
    header("location: index.php");
}

function editBook($bookId, $data1, $data2, $data3, $data4, $data5, $data6){
    global $link;
    $sql = "UPDATE books 
            SET title = '$data1', description = '$data2', pubyear = $data3, price = $data4
            WHERE id = $bookId";
    mysqli_query($link, $sql) or die(mysqli_error($link));

//    update genres
    $sql = "DELETE FROM books_genres
		WHERE id_book = $bookId";
    mysqli_query($link, $sql) or die(mysqli_error($link));
    foreach ($data5 as $id_genre){
        $sql = "INSERT INTO books_genres 
                (id_genre, id_book) 
                VALUE 
                ($id_genre, $bookId)";
        mysqli_query($link, $sql) or die(mysqli_error($link));
    }

//    update authors
    $sql = "DELETE FROM books_authors
		WHERE id_book = $bookId";
    mysqli_query($link, $sql) or die(mysqli_error($link));
    foreach ($data6 as $id_author){
        $sql = "INSERT INTO books_authors
                (id_author, id_book)
                VALUE
                ($id_author, $bookId)";
        mysqli_query($link, $sql) or die(mysqli_error($link));
    }
    header("location: index.php");
}

function ajax_filter_search($select){
    if (empty($select['genre']))
        $select['genre'] = 0;

    if (empty($select['author']))
        $select['author'] = 0;

    global $link;
    $sql = "SELECT DISTINCT books.id, title, pubyear, price
            FROM books
            INNER JOIN books_genres ON books_genres.id_book = books.id
            INNER JOIN books_authors ON books_authors.id_book = books.id
            WHERE (books_genres.id_genre LIKE $select[genre] OR 0 = $select[genre])
            AND (books_authors.id_author LIKE $select[author] OR 0 = $select[author])
            ORDER BY title";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    return($result);
}

function bookDetailById($bookId){
    global $link;
    $sql = "SELECT * FROM books
            WHERE books.id = $bookId";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    return($result);
}
function bookGenresById($bookId){
    global $link;
    $sql = "SELECT name AS genres_name, books_genres.id_genre AS genres_id
            FROM genres
            INNER JOIN books_genres ON books_genres.id_genre = genres.id
            WHERE id_book = $bookId";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    return($result);
}
function bookAuthorsById($bookId){
    global $link;
    $sql = "SELECT name AS authors_name, books_authors.id_author AS authors_id
            FROM authors
            INNER JOIN books_authors ON books_authors.id_author = authors.id
            WHERE id_book = $bookId";
    $result = mysqli_query($link, $sql) or die(mysqli_error($link));
    return($result);
}

function delBook($bookId){
    global $link;
    $sql = "DELETE FROM books
		WHERE id = $bookId";
    mysqli_query($link, $sql) or die(mysqli_error($link));
    header("location: index.php");
}

?>