-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 03, 2020 at 12:15 PM
-- Server version: 5.7.20
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myeshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `name`) VALUES
(1, 'Tillie Hansen'),
(2, 'Abdurrahman Wang'),
(3, 'Amisha Morley'),
(4, 'Klara Stark'),
(5, 'Aiysha Ewing'),
(6, 'Jokubas Burt'),
(7, 'Fletcher Meadows'),
(8, 'David Sedaris'),
(9, 'Lewis Frumkes');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `pubyear` int(11) NOT NULL DEFAULT '0',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `description`, `pubyear`, `price`) VALUES
(1, 'Always Ask A Man: The Key To Femininity', 'Being a white woman in the 1960s must’ve been exhausting.At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 1960, '555.55'),
(2, 'And on the Eighth Day God Created Hairdressers', 'We’re all blessed for this. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 2000, '50.00'),
(3, 'The Bible and Flying Saucers', 'Together forever. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 2010, '456.00'),
(4, 'Brainwashing is a Cinch!', 'Both self-help and a how-to. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 1999, '10.99'),
(5, 'Carma Sutra: The Auto‑Erotic Handbook', 'It even gets model specific. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 1979, '10.00'),
(6, 'Catflexing: A Catlover’s Guide to Weightlifting, Aerobics & Stretching', 'No cat would agree to this. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 1944, '15.00'),
(7, 'The Commuter Pig Keeper', 'This isn’t about piggy banks. At Artfully Yours School of Fine Art in Sherwood Park Alberta, our classes are focused on helping you develop at your own pace and level with an emphasis on technique and creativity for fabulous results.\r\nOur program is designed on helping you grow using the old masters\' art techniques.\r\nOur classes are $104 per month and geared towards the individual.', 1977, '9.00'),
(43, 'Me Talk Pretty One Day', 'Me Talk Pretty One Day, published in 2000, is a bestselling collection of essays by American humorist David Sedaris. The book is separated into two parts', 2000, '50.00'),
(44, 'How to Raise Your I.Q. by Eating Gifted Children', 'Lewis Burke Frumkes, one of America\'s very best satirists, sharpens his pen on the fads, fears, and fashions of the urban landscape. Here are 49 hilarious ways to cope with them. Explore the benefits of aerobic typing.', 1983, '50.00');

-- --------------------------------------------------------

--
-- Table structure for table `books_authors`
--

CREATE TABLE `books_authors` (
  `id` int(11) NOT NULL,
  `id_author` int(11) DEFAULT NULL,
  `id_book` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books_authors`
--

INSERT INTO `books_authors` (`id`, `id_author`, `id_book`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 5, 3),
(4, 7, 4),
(5, 3, 5),
(6, 7, 5),
(7, 1, 6),
(8, 2, 7),
(9, 5, 7),
(10, 3, 7),
(34, 8, 43),
(35, 9, 44);

-- --------------------------------------------------------

--
-- Table structure for table `books_genres`
--

CREATE TABLE `books_genres` (
  `id` int(11) NOT NULL,
  `id_genre` int(11) DEFAULT NULL,
  `id_book` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books_genres`
--

INSERT INTO `books_genres` (`id`, `id_genre`, `id_book`) VALUES
(1, 4, 1),
(2, 2, 1),
(3, 1, 2),
(4, 2, 3),
(5, 2, 4),
(6, 4, 5),
(7, 1, 5),
(8, 3, 6),
(9, 5, 6),
(10, 2, 7),
(11, 5, 7),
(39, 6, 43),
(40, 7, 43),
(41, 7, 44);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Fantastic'),
(2, 'Scientific'),
(3, 'Erotic'),
(4, 'Detective'),
(5, 'Tales'),
(6, 'Essay'),
(7, 'Humour');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books_authors`
--
ALTER TABLE `books_authors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_author` (`id_author`),
  ADD KEY `books_a` (`id_book`);

--
-- Indexes for table `books_genres`
--
ALTER TABLE `books_genres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_g` (`id_book`),
  ADD KEY `genres` (`id_genre`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `books_authors`
--
ALTER TABLE `books_authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `books_genres`
--
ALTER TABLE `books_genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books_authors`
--
ALTER TABLE `books_authors`
  ADD CONSTRAINT `authors` FOREIGN KEY (`id_author`) REFERENCES `authors` (`id`),
  ADD CONSTRAINT `books_a` FOREIGN KEY (`id_book`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `books_genres`
--
ALTER TABLE `books_genres`
  ADD CONSTRAINT `books_g` FOREIGN KEY (`id_book`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `genres` FOREIGN KEY (`id_genre`) REFERENCES `genres` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
