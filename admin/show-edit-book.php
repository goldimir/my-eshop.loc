<?php
require "../inc/connect-bd.php";
require "../inc/lib.php";

$bookId = clearData($_POST['id'],'int');

$resultBookDetails = bookDetailById($bookId);
$bookView = mysqli_fetch_assoc( $resultBookDetails );

$resultBookGenres = bookGenresById($bookId);
while ($view = mysqli_fetch_assoc($resultBookGenres)) {
    $selGenres[] = $view['genres_id'];
}

$resultBookAuthors = bookAuthorsById($bookId);
while ($view = mysqli_fetch_assoc($resultBookAuthors)) {
    $selAuthors[] = $view['authors_id'];
}
?>

<!--Modal Edit Book-->
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Edit book</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <form id="form-edit-book" action="edit-book.php" method="post">
        <input name="id" class="id" type="hidden" value="<?=$bookId?>" />
        <div class="modal-body">
            <div class="form-group"><input class="form-control title" id="title" type="text" name="title" placeholder="Title" value="<?=$bookView["title"]?>"></div>
            <div class="form-row">
                <div class="form-group col">
                    <input class="form-control pubyear" id="pubyear" type="text" name="pubyear" placeholder="Published" value="<?=$bookView["pubyear"]?>">
                </div>
                <div class="form-group col">
                    <div class="input-group wr-price">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input class="form-control price" id="price" type="text" name="price" placeholder="Price" value="<?=$bookView["price"]?>">
                        <div class="input-group-append">
                            <span class="input-group-text">.00</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group"><textarea class="form-control description" id="description" name="description" rows="7" placeholder="Description"><?=$bookView["description"]?></textarea></div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <h5>Select Genre:</h5>
                    <select class="form-control genres" id="genres" name="genres[]" multiple>
                        <?php
                        $resultGenres = selectAll('genres');
                        while ($view = mysqli_fetch_assoc($resultGenres)){
                            $selected = (in_array($view['id'], $selGenres)) ? $selected = 'selected' : '';
                            echo "<option $selected value='$view[id]'>$view[name]</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <h5>Select Author:</h5>
                    <select class="form-control authors" id="authors" name="authors[]" multiple>
                        <?php
                        $resultAuthors = selectAll('authors');
                        while ($view = mysqli_fetch_assoc($resultAuthors)){
                            $selected = (in_array($view['id'], $selAuthors)) ? $selected = 'selected' : '';
                            echo "<option $selected value='$view[id]'>$view[name]</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="form-group">
                <button type="submit" id="edit-book-submit" class="btn btn-primary book-submit">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </form>
</div>
