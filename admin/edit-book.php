<?php
require "../inc/connect-bd.php";
require "../inc/lib.php";

$bookId      = clearData($_POST['id'],'int');
$title       = clearData($_POST['title']);
$description = clearData($_POST['description']);
$pubyear     = clearData($_POST['pubyear'],'int');
$price       = clearData($_POST['price'],'prc');
$genres      = clearData($_POST['genres'], 'arr');
$authors     = clearData($_POST['authors'], 'arr');

editBook($bookId, $title, $description, $pubyear, $price, $genres, $authors);

?>