<?php
	require "../inc/connect-bd.php";
	require "../inc/lib.php";
?>
<table class="table table-sm table-striped table-hover table-bordered list-book">
    <thead class="thead-light">
        <tr>
            <th>Title</th>
            <th>Published</th>
            <th>Price</th>
            <th>Purchase</th>
        </tr>
    </thead>
    <?php
    $selectResult = ajax_filter_search( $_POST['term'] );
    while($view = mysqli_fetch_assoc( $selectResult )){
  ?>
        <tr>
            <td><?=$view["title"]?></td>
            <td><?=$view["pubyear"]?></td>
            <td>$<?=$view["price"]?></td>
            <td>
                <a class="btn btn-primary btn-sm buy-book" data-toggle="tooltip" data-placement="top" href="../inc/purchase-book.php?id=<?=$view["id"]?>" title="Buy"><i class="fas fa-shopping-bag"></i></a>
                <span class="edit-book-btn" data-toggle="modal" data-target="#modal-edit-book" data-id="<?=$view["id"]?>">
                    <span class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></span>
                </span>
                <span class="del-book-btn" data-toggle="modal" data-target="#modal-del-book" data-id="<?=$view["id"]?>">
                    <span class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></span>
                </span>
            </td>
        </tr>
        <?php
    }
    ?>
</table>

<!--Modal Delete book-->
<div class="modal fade" id="modal-del-book" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Book?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <a href="" class="btn btn-danger btn-lg"><i class="fas fa-trash-alt"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-book" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document"></div>
</div>