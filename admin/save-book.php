<?php
	require "../inc/connect-bd.php";
	require "../inc/lib.php";
	
	$title       = clearData($_POST['title']);
	$description = clearData($_POST['description']);
	$pubyear     = clearData($_POST['pubyear'],'int');
	$price       = clearData($_POST['price'],'prc');
    $genres      = clearData($_POST['genres'], 'arr');
    $authors     = clearData($_POST['authors'], 'arr');

	addBook($title, $description, $pubyear, $price, $genres, $authors);
?>