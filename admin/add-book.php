<p><button class="btn btn-primary" data-toggle="modal" data-target="#addBookModalCenter">Add New Book</button></p>

<div class="modal fade" id="addBookModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLongTitle">Add new book</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-add-book" action="save-book.php" method="post">
                <div class="modal-body">
                    <div class="form-group"><input class="form-control title" id="title" type="text" name="title" placeholder="Title"></div>
                    <div class="form-row">
                        <div class="form-group col">
                            <input class="form-control pubyear" id="pubyear" type="text" name="pubyear" placeholder="Published">
                        </div>
                        <div class="form-group col">
                            <div class="input-group wr-price">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input class="form-control price" id="price" type="text" name="price" placeholder="Price">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group"><textarea class="form-control description" id="description" name="description" rows="7" placeholder="Description"></textarea></div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h5>Select Genre:</h5>
                            <select class="form-control genres" id="genres" name="genres[]" multiple>
                                <?php
                                $resultGenres = selectAll('genres');
                                while ($view = mysqli_fetch_assoc($resultGenres)){
                                    echo "<option value='$view[id]'>$view[name]</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <h5>Select Author:</h5>
                            <select class="form-control authors" id="authors" name="authors[]" multiple>
                                <?php
                                $resultAuthors = selectAll('authors');
                                while ($view = mysqli_fetch_assoc($resultAuthors)){
                                    echo "<option value='$view[id]'>$view[name]</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <button type="submit" id="add-book-submit" class="btn btn-primary book-submit">Add</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>