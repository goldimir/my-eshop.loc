<?php require_once 'layout/header.php'; ?>
    <div class="row">
        <div class="col">
            <h1>Books Shop</h1>
            <div class="login-box">
                <p><a class="btn btn-danger" href="/admin/index.php">Log In to Admin panel</a></p>
                <p><b>Login:</b> <span class="yellow">admin</span><br><b>Pass:</b> <span class="yellow">12345</span></p>
                <?php
                $dir = dirname(__FILE__);
                echo "<p><b>Full path to this dir:</b> <span class='yellow'>" . $dir . "</span></p>";
                echo "<p><b>Full path to a .htpasswd file in this dir:</b> <span class='yellow'>" . $dir . "/.htpasswd" . "</span></p>";
                ?>
            </div>
            <div class="wrap-vg">
                <div class="filter-books form-stadium">
                    <form class="vg-filter">
                        <div class="form-group">
                            <select name="genres[]" class="form-control select-filter selectbasic">
                                <option selected data-book="genre" value="">Genre</option>
                                <?php
                                $resultGenres = selectAll('genres');
                                while ($view = mysqli_fetch_assoc($resultGenres)){
                                    echo "<option data-book='genre' value='$view[id]'>$view[name]</option>";
                                }
                                ?>
                            </select>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="form-group">
                            <select name="authors[]" class="form-control select-filter selectbasic">
                                <option selected data-book="author" value="">Author</option>
                                <?php
                                $resultAuthors = selectAll('authors');
                                while ($view = mysqli_fetch_assoc($resultAuthors)){
                                    echo "<option data-book='author' value='$view[id]'>$view[name]</option>";
                                }
                                ?>
                            </select>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <input type="submit" value="Reset" class="form-control reset-btn btn btn-outline-dark">
                    </form>
                </div>
                <div class="books-catalog"></div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            form.trigger('change');
        });
    </script>

<?php require_once 'layout/footer.php'; ?>